const getSum = (str1, str2) => {

    if (!(typeof str1 === 'string' && typeof str2 === 'string')) { return false; }

    if (str1.length == 0) { str1 = 0; }
    else if (isNaN(str1)) { return false; }
    else { str1 = parseFloat(str1);}
    if (str2.length == 0) { str2 = 0; }
    else if (isNaN(str2)) { return false; }
    else { str2 = parseFloat(str2);}

    return String(str1+str2);

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {

    let posts = 0;
    let comments = 0;

    for (let obj of listOfPosts) {
        if (obj.author == authorName) { posts++; }
        if (obj.comments != null) {
            for (const comment of obj.comments) {
                if (comment.author == authorName) { comments++; }
            }
        }
        
    }

    return 'Post:'+posts+',comments:'+comments;
};

const tickets=(people)=> {

    let sum = 0;

    for (let customer of people) {
        if (customer - sum > 25) return 'NO';
        sum += 25;
    }

    return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
